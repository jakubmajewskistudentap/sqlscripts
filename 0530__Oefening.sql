USE modernways;
INSERT INTO boeken
(Familienaam, Titel, Verschijningsjaar, Categorie)
VALUES
('?', 'Beowulf', '0975', 'Mythologie'),
('Ovidius', 'Metamorfosen', '8', 'Mythologie');
SELECT * FROM boeken 
	WHERE Verschijningsjaar < '0976'
	OR length(Verschijningsjaar) <= 3 
		AND Verschijningsjaar < '976';