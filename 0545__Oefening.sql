USE modernways;
SELECT Artiest, SUM(Aantalbeluisteringen) AS `totaal aantal beluisteringen`
FROM liedjes
WHERE LENGTH(Artiest) >= 10
GROUP BY Artiest
HAVING SUM(Aantalbeluisteringen) >= 100;