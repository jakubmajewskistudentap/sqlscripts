USE modernways;
SET SQL_SAFE_UPDATES = 0;
ALTER TABLE liedjes ADD COLUMN `Genre` VARCHAR(20);
UPDATE liedjes SET Genre = 'Hard Rock'
WHERE Artiest = 'Led Zeppelin' OR Artiest = 'Van Halen';
SET SQL_SAFE_UPDATES = 1;