USE modernways;
SELECT Artiest, SUM(Aantalbeluisteringen) AS `Totaal aantal beluisteringen`
FROM liedjes
GROUP BY Artiest
HAVING LENGTH(Artiest) >= 10;