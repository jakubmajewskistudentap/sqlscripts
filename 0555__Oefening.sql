USE modernways;
SELECT huisdieren.Naam, huisdieren.Leeftijd, huisdieren.Soort, huisdieren.Geluid, huisdieren.Id, baasjes.Naam, baasjes.Id, baasjes.Huisdieren_Id
FROM huisdieren CROSS JOIN baasjes;